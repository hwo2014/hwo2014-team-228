package main

import (
	"log"
)

// Global state
var (
	throttle        float32
	oldThrottle     float32
	oldSlipAngle    float64
	oldOldSlipAngle float64
	switchLane      string // Left, Right or empty
	onStraight      bool
	oldOnStraight   bool
)

func init_brain() {
	throttle = 0.5
	log.Printf("T %f\n", throttle)
}

func slipAngle_from_throttle_data(data interface{}) float64 {
	dataList, ok := data.([]interface{})
	if !ok {
		log.Println("1: Could not cast from interface in slipAngle_from_throttle_data")
		return 0.0
	}
	if len(dataList) < 1 {
		log.Println("Too little data in calculate_throttle")
		return 0.0
	}
	m, ok := dataList[0].(map[string]interface{})
	if !ok {
		log.Println("2: Could not cast from interface in slipAngle_from_throttle_data")
		return 0.0
	}
	slipAngle, ok := m["angle"].(float64)
	if !ok {
		log.Println("3: Could not cast from interface in slipAngle_from_throttle_data")
		return 0.0
	}
	return slipAngle
}

func abs(x float64) float64 {
	if x < 0 {
		return -x
	}
	return x
}

func calculate_throttle(data interface{}) float32 {
	slipAngle := slipAngle_from_throttle_data(data)
	log.Println("slipAngle:", slipAngle)

	slipDiff := abs(slipAngle - oldOldSlipAngle)
	oldOldSlipAngle = oldSlipAngle
	oldSlipAngle = slipAngle

	log.Println("slipDiff:", slipDiff)

	// Translate from slipDiff to throttle

	if (throttle == 0) && (slipDiff == 0) {
		// A reasonable start value
		throttle = 0.8
	}

	// Control the throttle based on the slippage

	if slipDiff > 5 { // Things are getting out of hand, slow down drastically
		if (slipAngle > 5) || (slipAngle < -5) {
			log.Println("SPINNING, PROBABLY TOO LATE!")
			// We're spinning!
			throttle = 0.1
		}
	} else if slipDiff > 0.7 { // Things are getting out of hand, slow down quite a bit
		// This is the early warning for crashing
		if (slipAngle > 0.7) || (slipAngle < -0.7) {
			log.Println("COULD START SPINNING")
			// This could be bad
			throttle *= 0.1
		} else {
			log.Println("WEAK WATCH OUT")
			// Not looking too bad. Early warning. Slow down quite a bit!
			throttle *= 0.2
		}
	} else if slipDiff < 0.1 { // Things are fine and dandy, increase the throttle gradually
		throttle *= 1.1
	}

	// If we are likely to be on a straight part, go a bit faster
	if (slipAngle < 0.02) && (slipAngle > -0.02) {
		if throttle >= 0.78 {
			throttle = 0.78
		}
	} else if (slipAngle < 0.04) && (slipAngle > -0.04) {
		if throttle >= 0.75 {
			throttle = 0.75
		}
	} else {
		if throttle >= 0.72 {
			throttle = 0.72
		}
	}

	// Lane switching, same direction as the turn we are in
	if (slipAngle < 0.03) && (slipAngle > -0.03) {
		log.Println("straight")
		switchLane = ""
		oldOnStraight = onStraight
		onStraight = true
	} else if slipAngle > 0 {
		log.Println("right turn")
		switchLane = "Right"
	} else if slipAngle < 0 {
		log.Println("left turn")
		switchLane = "Left"
	}

	// Don't consider switching lanes when the going is rough
	if slipDiff >= 1.0 { // "threshold for skipping switching lanes"
		switchLane = ""
	}

	// If the last position was not straight, but this one is, speed up a bit extra
	if !oldOnStraight && onStraight {
		log.Println("ENTERING STRAIGHT")
		throttle *= 1.1
		if throttle < 0.8 {
			throttle = 0.8
		}
	}

	if oldOnStraight && !onStraight {
		log.Println("EXITING STRAIGHT")
		throttle *= 0.7
	}

	// Check for extreme values and clamp

	if throttle > 0.81 {
		throttle = 0.81
	} else if throttle <= 0.4 {
		throttle = 0.4
	}

	log.Println("throttle", throttle)
	log.Println()

	return throttle
}

func get_switchLane() string {
	return switchLane
}

func should_turbo(data interface{}) bool {
	return false
}
